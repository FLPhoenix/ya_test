package ttask.events;

import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>
 * Thread-safe implementation of the event counter.
 * </p>
 * <p>
 * <p>
 * Works with a time resolution of 1 second. Each event to be taken into
 * account just increments the atomic counter, where the 1 and only one
 * counter exists for each second in the history.
 * The second is identified by its number from 01/01/1970.
 * </p>
 * <p>
 * All counters for each second are kept in thread-safe sorted map.
 * Having the map sorted allows quickly calculate the statistics for a
 * given period of time, and easily clean all the records which are out
 * of the scope (happened earlier than 1 day ago).
 * </p>
 * <p>
 * Maintenance is done via task, scheduled each 5 minutes after 1 day of work.
 * That means the map usually shouldn't have more than 86400 + 300 KV pairs.
 * Failure of the maintenance task is not critical at all, it's really enough
 * to be executed at least once a day.
 * </p>
 */
public class ThreadSafeEventCounter implements EventCounter {
    private static final int SEC_IN_MIN = 60;
    private static final int SEC_IN_HOUR = 60 * SEC_IN_MIN;
    private static final int SEC_IN_DAY = 24 * SEC_IN_HOUR;

    /**
     * A map number_of_a_second -> counter for this second.
     */
    private final ConcurrentNavigableMap<Long, AtomicInteger> counters = new ConcurrentSkipListMap<>();

    /**
     * Thread pool for running the maintaining task.
     */
    private final ScheduledThreadPoolExecutor maintenanceExecutor = new ScheduledThreadPoolExecutor(1);

    public ThreadSafeEventCounter() {
        // schedule the maintenance task
        maintenanceExecutor.scheduleAtFixedRate(new CountersMaintainer(), 60 * 24, 5, TimeUnit.MINUTES);
    }

    @Override
    public void accept() {
        // get a number of a current second
        long secNumber = System.currentTimeMillis() / 1000;

        // get the counter for the particular second, or create one if it doesn't exist
        AtomicInteger currentCounter = counters.putIfAbsent(secNumber, new AtomicInteger());

        // update the counter
        if (currentCounter != null) {
            currentCounter.incrementAndGet();
        } else {
            counters.get(secNumber).incrementAndGet();
        }
    }

    @Override
    public synchronized int getLastMinuteCount() {
        return getLastNSecCount(SEC_IN_MIN);
    }

    @Override
    public synchronized int getLastHourCount() {
        return getLastNSecCount(SEC_IN_HOUR);
    }

    @Override
    public synchronized int getLastDayCount() {
        return getLastNSecCount(SEC_IN_DAY);
    }


    /**
     * Calculates total count of events for the given number of seconds
     *
     * @param numberOfSec number of seconds to look in history
     * @return sum of all counters for last numberOfSec seconds
     */
    private int getLastNSecCount(int numberOfSec) {
        // calculate the second which ends the N-sec frame
        long lastSecond = System.currentTimeMillis() / 1000 - numberOfSec;

        // get the view to records done after it
        ConcurrentNavigableMap<Long, AtomicInteger> secondsToCount = counters.tailMap(lastSecond);

        // calculate the sum
        int sum = 0;
        for (Long sec : secondsToCount.keySet()) {
            sum += secondsToCount.get(sec).get();
        }

        return sum;
    }

    /**
     * A task to maintain the counters map; clears every record which is older than 1 day.
     */
    private class CountersMaintainer implements Runnable {

        @Override
        public void run() {
            long lastSecondOfTheDay = System.currentTimeMillis() / 1000 - SEC_IN_DAY;
            counters.headMap(lastSecondOfTheDay).clear();
        }
    }
}
