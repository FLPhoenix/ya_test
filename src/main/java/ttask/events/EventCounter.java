package ttask.events;

/**
 * The counter interface.
 */
public interface EventCounter {

    /**
     * Take an event into account.
     */
    void accept();

    /**
     * @return number of events for the last minute.
     */
    int getLastMinuteCount();

    /**
     * @return number of events for the last hour.
     */
    int getLastHourCount();

    /**
     * @return number of events for the last day.
     */
    int getLastDayCount();
}
