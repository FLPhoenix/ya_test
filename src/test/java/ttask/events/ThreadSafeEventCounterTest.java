package ttask.events;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadSafeEventCounterTest {
    private ExecutorService executorService;

    @Before
    public void init() {
        executorService = Executors.newFixedThreadPool(8);
    }

    @Test
    public void testSingleThread() throws InterruptedException, IOException {
        ThreadSafeEventCounter counter = new ThreadSafeEventCounter();
        executorService.submit(new Worker(counter, 10000));
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);

        Assert.assertEquals(10000, counter.getLastMinuteCount());
        Assert.assertEquals(10000, counter.getLastHourCount());
        Assert.assertEquals(10000, counter.getLastDayCount());
    }

    @Test
    public void testDoubleThread() throws InterruptedException, IOException {
        ThreadSafeEventCounter counter = new ThreadSafeEventCounter();
        executorService.submit(new Worker(counter, 10000));
        executorService.submit(new Worker(counter, 10000));
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);

        Assert.assertEquals(20000, counter.getLastMinuteCount());
        Assert.assertEquals(20000, counter.getLastHourCount());
        Assert.assertEquals(20000, counter.getLastDayCount());
    }

    @Test
    public void testQuarterThread() throws InterruptedException, IOException {
        ThreadSafeEventCounter counter = new ThreadSafeEventCounter();
        executorService.submit(new Worker(counter, 10000));
        executorService.submit(new Worker(counter, 10000));
        executorService.submit(new Worker(counter, 10000));
        executorService.submit(new Worker(counter, 10000));
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);

        Assert.assertEquals(40000, counter.getLastMinuteCount());
        Assert.assertEquals(40000, counter.getLastHourCount());
        Assert.assertEquals(40000, counter.getLastDayCount());
    }

    @Test
    public void testQuarterThreadWithSleep() throws InterruptedException, IOException {
        ThreadSafeEventCounter counter = new ThreadSafeEventCounter();
        for (int i = 0; i < 10; i++) {
            executorService.submit(new Worker(counter, 10000));
            executorService.submit(new Worker(counter, 10000));
            executorService.submit(new Worker(counter, 10000));
            executorService.submit(new Worker(counter, 10000));
            Thread.currentThread().sleep(500);
        }
        Assert.assertEquals(400000, counter.getLastMinuteCount());
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);
    }

    @Test
    public void testEightThreadWithSleep() throws InterruptedException, IOException {
        ThreadSafeEventCounter counter = new ThreadSafeEventCounter();
        for (int i = 0; i < 10; i++) {
            for (int k = 0; k < 8; k++) {
                executorService.submit(new Worker(counter, 10000));
            }
            Thread.currentThread().sleep(500);
        }
        Assert.assertEquals(800000, counter.getLastMinuteCount());
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);
    }

    @Test
    public void testEightThreadWithRandomSleep() throws InterruptedException, IOException {
        ThreadSafeEventCounter counter = new ThreadSafeEventCounter();
        Random random = new Random(376782134);
        for (int i = 0; i < 10; i++) {
            for (int k = 0; k < 8; k++) {
                executorService.submit(new Worker(counter, 10000));
                Thread.currentThread().sleep(random.nextInt(100));
            }
            Thread.currentThread().sleep(random.nextInt(100));
        }
        Assert.assertEquals(800000, counter.getLastMinuteCount());
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);
    }

    @Test
    public void testEightThreadWithRandomlySleepingWorkers() throws InterruptedException, IOException {
        ThreadSafeEventCounter counter = new ThreadSafeEventCounter();
        Random random = new Random(2875364);
        for (int i = 0; i < 10; i++) {
            for (int k = 0; k < 8; k++) {
                executorService.submit(new SleepingWorker(counter, 10, 100));
            }
            Thread.currentThread().sleep(random.nextInt(100));
        }
        executorService.shutdown();
        executorService.awaitTermination(30, TimeUnit.SECONDS);
        Assert.assertEquals(800, counter.getLastMinuteCount());
    }

    @Test
    public void testEightThreadWithSleepHeavilyLoaded() throws InterruptedException, IOException {
        ThreadSafeEventCounter counter = new ThreadSafeEventCounter();
        for (int i = 0; i < 10; i++) {
            for (int k = 0; k < 8; k++) {
                executorService.submit(new Worker(counter, 10_000_000));
            }
            Thread.currentThread().sleep(500);
        }
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);
        Assert.assertEquals(800_000_000, counter.getLastMinuteCount());
    }

    static class Worker implements Runnable {
        EventCounter counter;
        int numEvents = 0;

        Worker(EventCounter counter, int numEvents) {
            this.counter = counter;
            this.numEvents = numEvents;
        }

        @Override
        public void run() {
            for (int i = 0; i < numEvents; i++) {
                counter.accept();
            }
        }
    }

    static class SleepingWorker implements Runnable {
        EventCounter counter;
        int numEvents = 0;
        int sleepIntervalLimit = 1;
        Random random = new Random();

        SleepingWorker(EventCounter counter, int numEvents, int sleepIntervalLimit) {
            this.counter = counter;
            this.numEvents = numEvents;
            this.sleepIntervalLimit = sleepIntervalLimit;
        }

        @Override
        public void run() {
            for (int i = 0; i < numEvents; i++) {
                counter.accept();
                try {
                    Thread.currentThread().sleep(random.nextInt(sleepIntervalLimit));
                } catch (InterruptedException e) {
                    continue;
                }
            }
        }
    }

    @After
    public void finish() {
        if (executorService != null) {
            executorService.shutdownNow();
        }
    }
}
